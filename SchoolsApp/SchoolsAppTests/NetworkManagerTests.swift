//
//  NetworkManagerTests.swift
//  SchoolsAppTests
//
//  Created by Raul Cortes on 1/18/23.
//

import XCTest
@testable import SchoolsApp

class NetworkManagerTests: XCTestCase {
    var network: NetworkManager?
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        self.network = NetworkManager(session: MockURLSession())
    }
    
    override func tearDownWithError() throws {
        self.network = nil
        try super.tearDownWithError()
    }
    
    func testSuccessfullyFetchDecodableData() {
        // Arrange
        let expectation = XCTestExpectation(description: "Successfully fetching the model data")
        let url = URL(string: "https://Example.com/success")
        var highSchools: SchoolList = []
        
        // Act
        self.network?.fetchHighSchoolList(url: url, completion: { (result: Result<SchoolList, Error>) in
            switch result {
            case .success(let page):
                highSchools = page
                expectation.fulfill()
            case .failure(let err):
                XCTFail("Failed to decode the model. Got Error: \(err)")
            }
        })
        wait(for: [expectation], timeout: 3)
        
        // Assert
        XCTAssertEqual(highSchools.count, 478)
        XCTAssertEqual(highSchools.first?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(highSchools.first?.dbn, "01M292")
    }
    
    func testFailedForBadURL() {
        // Arrange
        let expectation = XCTestExpectation(description: "URL Failed to be converted")
        var err: NetworkError?
    
        // Act
        self.network?.fetchHighSchoolList(url: nil, completion: { (result: Result<SchoolList, Error>) in
            switch result {
            case .success:
                XCTFail("Failed at failing on URL")
            case .failure(let error):
                err = error as? NetworkError
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 3)
        
        guard let errorInfo = err else{
            return
        }
        
        // Assert
        XCTAssertEqual(errorInfo.localizedDescription, NetworkError.badURL.localizedDescription)
    }
    
    func testFailedForGeneralError() {
        // Arrange
        let expectation = XCTestExpectation(description: "General Failure")
        let url = URL(string: "https://Example.com/generalError")
        var err: NetworkError?
    
        // Act
        self.network?.fetchHighSchoolList(url: url, completion: { (result: Result<SchoolList, Error>) in
            switch result {
            case .success:
                XCTFail("Failed at getting general Error")
            case .failure(let error):
                err = error as? NetworkError
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 3)
        
        // Assert
        let error = NSError(domain: "Test", code: 0, userInfo: nil)
        
        guard let errorInfo = err else{
            return
        }
        XCTAssertEqual(errorInfo.localizedDescription, NetworkError.generalError(error).localizedDescription)
    }
    
    func testFailedForBadData() {
        // Arrange
        let expectation = XCTestExpectation(description: "Bad Data Failure")
        let url = URL(string: "https://Example.com/badData")
        var err: NetworkError?
    
        // Act
        self.network?.fetchHighSchoolList(url: url, completion: { (result: Result<SchoolList, Error>) in
            switch result {
            case .success:
                XCTFail("Failed at getting bad data")
            case .failure(let error):
                err = error as? NetworkError
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 3)
        
        guard let errorInfo = err else{
            return
        }
        // Assert
        XCTAssertEqual(errorInfo.localizedDescription, NetworkError.badData.localizedDescription)
        
    }
    
    func testFailedForDecodeError() {
        // Arrange
        let expectation = XCTestExpectation(description: "Decode Failure")
        let url = URL(string: "https://Example.com/decodeFailure")
        var err: NetworkError?
    
        // Act
        self.network?.fetchHighSchoolList(url: url, completion: { (result: Result<SchoolList, Error>) in
            switch result {
            case .success:
                XCTFail("Failed at failing to decode")
            case .failure(let error):
                err = error as? NetworkError
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 3)
        
        guard let errorInfo = err else{
            return
        }
        
        // Assert
        XCTAssertEqual(errorInfo.localizedDescription, NetworkError.decodeError("The data couldn’t be read because it isn’t in the correct format.").localizedDescription)
        
    }

}
