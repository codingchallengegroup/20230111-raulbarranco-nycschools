//
//  MockURLSession.swift
//  SchoolsAppTests
//
//  Created by Raul Cortes on 1/18/23.
//

import Foundation
@testable import SchoolsApp

class MockURLSession: Session {
    func getData(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
            
            if url.absoluteString.contains("success") {
                let bundle = Bundle(for: MockURLSession.self)
                guard let path = bundle.path(forResource: "Sample", ofType: "json") else {
                    fatalError("Failed to fetch HighSchool sample payload")
                }
                let url = URL(fileURLWithPath: path)
                let data = try? Data(contentsOf: url)
                completion(data, nil, nil)
            } else if url.absoluteString.contains("generalError") {
                let error = NSError(domain: "Test", code: 0, userInfo: nil)
                completion(nil, nil, NetworkError.generalError(error))
            } else if url.absoluteString.contains("badData") {
                completion(nil, nil, nil)
            } else if  url.absoluteString.contains("decodeFailure") {
                completion(Data(), nil, nil)
            }
            
        }
    }
    
    
}
