//
//  TableViewCell.swift
//  SchoolsApp
//
//  Created by Raul Cortes on 1/18/23.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    static let reuseId = "\(TableViewCell.self)"

    lazy var sampleHSImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var hsName: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = label.font.withSize(10)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setUpUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpUI() {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 8
        
        stackView.addArrangedSubview(self.sampleHSImageView)
        stackView.addArrangedSubview(self.hsName)
        
        self.sampleHSImageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.sampleHSImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.contentView.addSubview(stackView)
        stackView.bindToSuperView(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8))
    }
    
    override func prepareForReuse() {
        self.sampleHSImageView.image = nil
        self.hsName.text = nil
    }

}



