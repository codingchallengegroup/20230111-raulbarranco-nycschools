//
//  DetailViewController.swift
//  SchoolsApp
//
//  Created by Raul Cortes on 1/18/23.
//

import UIKit

class DetailViewController: UIViewController {
    
    lazy var dbnLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = label.font.withSize(15)
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    lazy var schoolNameLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = label.font.withSize(15)
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    lazy var numofTestTakerLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = label.font.withSize(15)
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    lazy var readingScoreAvgLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = label.font.withSize(15)
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    lazy var mathScoreAvgLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = label.font.withSize(15)
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    lazy var writingScoreAvgLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = label.font.withSize(15)
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let schoolViewModel: SchoolViewModelType
    let index: Int
    
    init(schoolViewModel: SchoolViewModelType, index: Int) {
        self.schoolViewModel = schoolViewModel
        self.index = index
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.setupUI()
        
        self.dbnLabel.text = self.schoolViewModel.dbn(index: self.index)
        self.schoolNameLabel.text = self.schoolViewModel.nameOfSchool(index: self.index)
        self.numofTestTakerLabel.text = self.schoolViewModel.numOfTestTakers(index: self.index)
        self.readingScoreAvgLabel.text = self.schoolViewModel.readingSATScore(index: self.index)
        self.mathScoreAvgLabel.text = self.schoolViewModel.mathSATScore(index: self.index)
        self.writingScoreAvgLabel.text = self.schoolViewModel.writingSATScore(index: self.index)
    }
    
    func setupUI() {
        self.view.addSubview(self.dbnLabel)
        self.view.addSubview(self.schoolNameLabel)
        self.view.addSubview(self.numofTestTakerLabel)
        self.view.addSubview(self.readingScoreAvgLabel)
        self.view.addSubview(self.mathScoreAvgLabel)
        self.view.addSubview(self.writingScoreAvgLabel)
        
        self.schoolNameLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        self.schoolNameLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        self.schoolNameLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
        self.schoolNameLabel.bottomAnchor.constraint(equalTo: self.dbnLabel.topAnchor, constant: -10).isActive = true
        
        self.dbnLabel.topAnchor.constraint(equalTo: self.schoolNameLabel.bottomAnchor, constant: 10).isActive = true
        self.dbnLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        self.dbnLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
        self.dbnLabel.bottomAnchor.constraint(equalTo: self.numofTestTakerLabel.topAnchor, constant: -10).isActive = true
        
        self.numofTestTakerLabel.topAnchor.constraint(equalTo: self.dbnLabel.bottomAnchor, constant: 10).isActive = true
        self.numofTestTakerLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        self.numofTestTakerLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
        self.numofTestTakerLabel.bottomAnchor.constraint(equalTo: self.readingScoreAvgLabel.topAnchor, constant: -10).isActive = true
        
        self.readingScoreAvgLabel.topAnchor.constraint(equalTo: self.numofTestTakerLabel.bottomAnchor, constant: 10).isActive = true
        self.readingScoreAvgLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        self.readingScoreAvgLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
        self.readingScoreAvgLabel.bottomAnchor.constraint(equalTo: self.mathScoreAvgLabel.topAnchor, constant: -10).isActive = true
        self.mathScoreAvgLabel.topAnchor.constraint(equalTo: self.readingScoreAvgLabel.bottomAnchor, constant: 10).isActive = true
        self.mathScoreAvgLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        self.mathScoreAvgLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
        self.mathScoreAvgLabel.bottomAnchor.constraint(equalTo: self.writingScoreAvgLabel.topAnchor, constant: -10).isActive = true
        self.writingScoreAvgLabel.topAnchor.constraint(equalTo: self.mathScoreAvgLabel.bottomAnchor, constant: 10).isActive = true
        self.writingScoreAvgLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        self.writingScoreAvgLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 10).isActive = true
    }
    
}
