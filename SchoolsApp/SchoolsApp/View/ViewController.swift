//
//  ViewController.swift
//  SchoolsApp
//
//  Created by Raul Cortes on 1/18/23.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var highSchoolTableView: UITableView = {
        let table = UITableView(frame: .zero)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.dataSource = self
        table.delegate = self
        table.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.reuseId)
        return table
    }()
    
    var viewModel: SchoolViewModelType?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.setUpUI()
        self.viewModel = SchoolViewModel()
        self.viewModel?.bind {
            DispatchQueue.main.async {
                self.highSchoolTableView.reloadData()
            }
        }
    }
    
    private func setUpUI() {
        self.title = "List of Schools"
        self.view.backgroundColor = .white
        self.view.addSubview(self.highSchoolTableView)
        self.highSchoolTableView.bindToSuperView(insets: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        
    }

}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.numberOfSchools() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.reuseId, for: indexPath) as? TableViewCell else {
            return UITableViewCell()
        }
        
        //Sample Pics because API Didnt Have Picture
        if indexPath.row % 5 == 0 {
            cell.sampleHSImageView.image = UIImage(named: "Sample1")
        }
        else if indexPath.row % 5 == 1 {
            cell.sampleHSImageView.image = UIImage(named: "Sample2")
        }
        else if indexPath.row % 5 == 2 {
            cell.sampleHSImageView.image = UIImage(named: "Sample3")
        }
        else if indexPath.row % 5 == 3 {
            cell.sampleHSImageView.image = UIImage(named: "Sample4")
        }
        else {
            cell.sampleHSImageView.image = UIImage(named: "Sample5")
        }
        
        cell.hsName.text = self.viewModel?.nameOfSchool(index: indexPath.row)
    
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let viewModel = self.viewModel else { return }
        let detailVC = DetailViewController(schoolViewModel: viewModel, index: indexPath.row)
                
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}
