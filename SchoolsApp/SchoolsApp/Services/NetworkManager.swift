//
//  NetworkManager.swift
//  SchoolsApp
//
//  Created by Raul Cortes on 1/18/23.
//

import Foundation

protocol DataFetcher {
    func fetchHighSchoolList(url: URL?, completion: @escaping (Result<SchoolList, Error>) -> Void)
}

class NetworkManager {
    
    let session: Session
    
    init(session: Session = URLSession.shared) {
        self.session = session
    }
    
}

extension NetworkManager: DataFetcher {
    
    func fetchHighSchoolList(url: URL?, completion: @escaping (Result<SchoolList, Error>) -> Void) {
        
        guard let url = url else {
            completion(.failure(NetworkError.badURL))
            return
        }
        
        self.session.getData(url: url){ data, response, error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data else {
                completion(.failure(NetworkError.badData))
                return
            }

            do {
                let model = try JSONDecoder().decode(SchoolList.self, from: data)
                completion(.success(model))
            } catch {
                completion(.failure(NetworkError.decodeError(error.localizedDescription)))
            }
        }
        
    }
}

