//
//  NetworkParams.swift
//  SchoolsApp
//
//  Created by Raul Cortes on 1/18/23.
//

import Foundation

enum NetworkParams {
    case HighSchoolList
    
    var url: URL? {
        switch self {
        case .HighSchoolList:
            guard let urlComponents = URLComponents(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") else { return nil }
            
            return urlComponents.url
        }
    }
    
}
