//
//  SchoolViewModel.swift
//  SchoolsApp
//
//  Created by Raul Cortes on 1/18/23.
//

import Foundation

protocol SchoolViewModelType {
    func bind(updateHandler: @escaping () -> Void)
    func returnSchoolList() -> SchoolList
    func numberOfSchools() -> Int
    func nameOfSchool(index: Int) -> String
    func dbn(index: Int) -> String
    func numOfTestTakers(index: Int) -> String
    func readingSATScore(index: Int) -> String
    func mathSATScore(index: Int) -> String
    func writingSATScore(index: Int) -> String
}

class SchoolViewModel {
    var schoolList: SchoolList {
        didSet {
            self.updateHandler?()
        }
    }
    var updateHandler: (() -> Void)?
    let network: DataFetcher
    
    init(list: SchoolList = [], network: DataFetcher = NetworkManager()) {
        self.network = network
        self.schoolList = list
        self.network.fetchHighSchoolList(url: NetworkParams.HighSchoolList.url) {
            [weak self] (result: Result<SchoolList, Error>) in
            switch result {
            case .success(let list):
                self?.schoolList.append(contentsOf: list)
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension SchoolViewModel: SchoolViewModelType {
    
    func bind(updateHandler: @escaping () -> Void) {
        self.updateHandler = updateHandler
    }
    
    func returnSchoolList() -> SchoolList {
        return self.schoolList
    }
    
    func numberOfSchools() -> Int {
        return self.schoolList.count
    }
   
    func nameOfSchool(index: Int) -> String {
        return self.schoolList[index].schoolName
    }
    
    func dbn(index: Int) -> String {
        return "School DBN number: " + self.schoolList[index].dbn
    }
    
    func numOfTestTakers(index: Int) -> String {
        return "Number of SAT Takers: " + (self.schoolList[index].numOfSatTestTakers ?? "Not Available")
    }
    
    func readingSATScore(index: Int) -> String {
        return "Average Reading Scores: " + (self.schoolList[index].satCriticalReadingAvgScore ?? "Not Available")
    }
    
    func mathSATScore(index: Int) -> String {
        return "Average Math Scores: " + (self.schoolList[index].satMathAvgScore ?? "Not Available")
    }
    
    func writingSATScore(index: Int) -> String {
        return "Average Writing Scores: " + (self.schoolList[index].satWritingAvgScore ?? "Not Available")
    }
}
